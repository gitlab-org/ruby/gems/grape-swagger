# frozen_string_literal: true

require 'spec_helper'

describe '#887 avoid duplciate operationIds' do
  let(:app) do
    Class.new(Grape::API) do
      namespace :issue_887 do
        get 'item/{id}/-/search' do
          status 200
        end

        get 'item{id}/search' do
          status 200
        end

        get 'item/{id}/search' do
          status 200
        end
      end

      add_swagger_documentation format: :json
    end
  end
  let(:operation_ids) { subject['paths'].values.map{ |path| path['get']['operationId'] } }

  subject do
    get '/swagger_doc'
    JSON.parse(last_response.body)
  end

  specify do
    expect(operation_ids).to eq ["getIssue887ItemIdSearch", "getIssue887ItemIdSearch2", "getIssue887ItemIdSearch3"]
  end
end
